import { Injectable } from '@angular/core';
import { KeywordSelection } from '../interfaces/keyword-selection';
import { BehaviorSubject, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class KeywordSelectionsStoreService {
  private store: { [key: string]: boolean } = {};
  private subject = new BehaviorSubject<{ [key: string]: boolean }>({});

  getSubscription(callback: (store: { [key: string]: boolean }) => void): Subscription {
    return this.subject.subscribe(callback);
  }

  save(keywordSelection: KeywordSelection) {
    this.store[keywordSelection.title] = keywordSelection.isSelected;
    this.subject.next(this.store);
  }
}
