import { Injectable } from '@angular/core';
import { Keyword } from '../interfaces/keyword';
import { KeywordMatchType } from '../enums/keyword-match-type';
import { KeywordStatus } from '../enums/keyword-status';
import { SortingEvent } from '../../shared/components/sorting/sorting.component';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class KeywordsRequestService {
  private readonly keywordsList: Keyword[];

  constructor() {
    this.keywordsList = this.generateMockData();
  }

  fetchKeywords(): Observable<Keyword[]> {
    return of(this.keywordsList);
  }

  getSortedKeywords(sortingEvent: SortingEvent): Observable<Keyword[]> {
    return of(this.keywordsList.reverse());
  }

  private generateMockData(): Keyword[] {
    return new Array(20).fill(null).map((item, index) => {
      return {
        title: `Keyword title ${index + 1}`,
        group: `Keyword group ${index + 1}`,
        status: index % 2 === 0 ? KeywordStatus.enabled : KeywordStatus.disabled,
        matchType: KeywordMatchType.exact,
        suggestedBid: parseFloat((Math.random() * 10).toFixed(2)),
        keywordBid: parseFloat((Math.random() * 10).toFixed(2)),
        impressions: null,
        clicks: null,
        ctr: null,
        spend: null,
        cpc: null,
        orders: null,
        sales: null
      };
    });
  }
}
