import { Component, OnDestroy, OnInit } from '@angular/core';
import { Keyword } from '../../interfaces/keyword';
import { KeywordsRequestService } from '../../services/keywords-request.service';
import { SortingEvent } from '../../../shared/components/sorting/sorting.component';
import { KeywordSelectionsStoreService } from '../../services/keyword-selections-store.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-keywords-list-container',
  templateUrl: './keywords-list-container.component.html',
  styleUrls: ['./keywords-list-container.component.css']
})
export class KeywordsListContainerComponent implements OnInit, OnDestroy {
  listState: KeywordsListState = KeywordsListState.filter;
  keywordsList: Keyword[];

  private subscription = new Subscription();

  constructor(private keywordsRequestService: KeywordsRequestService,
              private keywordSelectionsStoreService: KeywordSelectionsStoreService) {
  }

  async ngOnInit() {
    this.subscription.add(
      this.keywordsRequestService.fetchKeywords()
        .subscribe(keywordsList => this.keywordsList = keywordsList)
    );

    this.subscription.add(
      this.subscription = this.keywordSelectionsStoreService
        .getSubscription(selectionsStore => {
          const isAnySelected = Object.keys(selectionsStore)
            .filter(key => selectionsStore[key]).length > 0;

          this.listState = isAnySelected
            ? KeywordsListState.actions
            : KeywordsListState.filter;
        })
    );
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  async onSortingChanged(sortingEvent: SortingEvent) {
    this.subscription.add(
      this.keywordsRequestService.getSortedKeywords(sortingEvent)
        .subscribe(keywordsList => this.keywordsList = keywordsList)
    );
  }

  isFilterState(): boolean {
    return this.listState === KeywordsListState.filter;
  }

  isActionsState(): boolean {
    return this.listState === KeywordsListState.actions;
  }
}

enum KeywordsListState {
  filter = 'filter',
  actions = 'actions'
}
