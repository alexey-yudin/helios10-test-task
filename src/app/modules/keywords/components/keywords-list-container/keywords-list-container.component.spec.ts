import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeywordsListContainerComponent } from './keywords-list-container.component';

describe('KeywordsListContainerComponent', () => {
  let component: KeywordsListContainerComponent;
  let fixture: ComponentFixture<KeywordsListContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KeywordsListContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeywordsListContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
