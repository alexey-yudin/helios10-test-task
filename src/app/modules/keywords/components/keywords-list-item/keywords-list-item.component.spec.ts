import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeywordsListItemComponent } from './keywords-list-item.component';

describe('KeywordsListItemComponent', () => {
  let component: KeywordsListItemComponent;
  let fixture: ComponentFixture<KeywordsListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KeywordsListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeywordsListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
