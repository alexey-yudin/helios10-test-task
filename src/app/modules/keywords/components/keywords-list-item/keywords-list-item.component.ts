import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Keyword } from '../../interfaces/keyword';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { KeywordSelectionsStoreService } from '../../services/keyword-selections-store.service';

@Component({
  selector: 'app-keywords-list-item',
  templateUrl: './keywords-list-item.component.html',
  styleUrls: ['./keywords-list-item.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class KeywordsListItemComponent implements OnInit {
  @Input() keyword: Keyword;
  formControl = new FormControl(false);
  private subscription = new Subscription;

  constructor(private keywordSelectionsStoreService: KeywordSelectionsStoreService) {
  }

  ngOnInit() {
    this.subscription.add(
      this.formControl
        .valueChanges
        .subscribe(isSelected => {
          this.keywordSelectionsStoreService.save({
            title: this.keyword.title,
            isSelected: isSelected
          });
        })
    );
  }
}
