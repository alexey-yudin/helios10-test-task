import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeywordsActionsComponent } from './keywords-actions.component';

describe('KeywordsActionsComponent', () => {
  let component: KeywordsActionsComponent;
  let fixture: ComponentFixture<KeywordsActionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KeywordsActionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeywordsActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
