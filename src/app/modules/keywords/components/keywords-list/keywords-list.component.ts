import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Keyword } from '../../interfaces/keyword';
import { SortingEvent } from '../../../shared/components/sorting/sorting.component';

@Component({
  selector: 'app-keywords-list',
  templateUrl: './keywords-list.component.html',
  styleUrls: ['./keywords-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class KeywordsListComponent {
  @Input() keywordsList: Keyword[];
  @Output() sortingChanged = new EventEmitter<SortingEvent>();

  activeField: string;

  onSortingChanged(sortingEvent: SortingEvent) {
    this.activeField = sortingEvent.fieldName;
    this.sortingChanged.emit(sortingEvent);
  }
}
