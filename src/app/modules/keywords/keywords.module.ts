import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KeywordsListContainerComponent } from './components/keywords-list-container/keywords-list-container.component';
import { KeywordsListComponent } from './components/keywords-list/keywords-list.component';
import { KeywordsFilterComponent } from './components/keywords-filter/keywords-filter.component';
import { KeywordsActionsComponent } from './components/keywords-actions/keywords-actions.component';
import { SharedModule } from '../shared/shared.module';
import { KeywordsListItemComponent } from './components/keywords-list-item/keywords-list-item.component';
import { ReactiveFormsModule } from '@angular/forms';
import { KeywordSelectionsStoreService } from './services/keyword-selections-store.service';

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    SharedModule
  ],
  declarations: [
    KeywordsListContainerComponent,
    KeywordsListComponent,
    KeywordsFilterComponent,
    KeywordsActionsComponent,
    KeywordsListItemComponent,
  ],
  exports: [
    KeywordsListContainerComponent,
  ],
  providers: [
    KeywordSelectionsStoreService
  ]
})
export class KeywordsModule {
}
