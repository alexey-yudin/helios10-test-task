import { KeywordStatus } from '../enums/keyword-status';
import { KeywordMatchType } from '../enums/keyword-match-type';

export interface Keyword {
  title: string;
  group: string;
  status: KeywordStatus;
  matchType: KeywordMatchType;
  suggestedBid: number;
  keywordBid: number;
  impressions: number | null;
  clicks: number | null;
  ctr: number | null;
  spend: number | null;
  cpc: number | null;
  orders: number | null;
  sales: number | null;
}
