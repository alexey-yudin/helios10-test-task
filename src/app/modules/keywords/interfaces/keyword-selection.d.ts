export interface KeywordSelection {
  title: string;
  isSelected: boolean;
}
