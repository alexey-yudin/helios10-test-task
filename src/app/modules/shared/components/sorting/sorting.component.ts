import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';

@Component({
  selector: 'app-sorting',
  templateUrl: './sorting.component.html',
  styleUrls: ['./sorting.component.css']
})
export class SortingComponent implements OnChanges {
  @Input() fieldName: string;
  @Input() activeFieldName: string;
  @Output() sortingStateChanged = new EventEmitter<SortingEvent>();

  sortingStateEnum = SortingState;
  sortingState: SortingState = SortingState.none;

  ngOnChanges() {
    if (this.activeFieldName !== this.fieldName) {
      this.sortingState = SortingState.none;
    }
  }

  onSortingChange(e: Event) {
    e.preventDefault();

    if (this.sortingState === SortingState.none) {
      this.sortingState = SortingState.asc;
    } else if (this.sortingState === SortingState.asc) {
      this.sortingState = SortingState.desc;
    } else if (this.sortingState === SortingState.desc) {
      this.sortingState = SortingState.none;
    }

    this.sortingStateChanged.emit({
      fieldName: this.fieldName,
      sortingState: this.sortingState,
    });
  }
}

export interface SortingEvent {
  fieldName: string;
  sortingState: SortingState;
}

export enum SortingState {
  asc = 'asc',
  desc = 'desc',
  none = 'none',
}
