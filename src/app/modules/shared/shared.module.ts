import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SortingComponent } from './components/sorting/sorting.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    SortingComponent
  ],
  exports: [
    SortingComponent
  ]
})
export class SharedModule { }
